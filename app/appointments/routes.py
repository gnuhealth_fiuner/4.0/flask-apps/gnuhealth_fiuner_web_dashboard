###########################################################
#Turnos en Pantalla: Mostramos los turnos que están siendo llamado,
# en atención y los siguientes
###########################################################

import json

from os import path, remove

from app import app, tryton
from app.appointments import blueprint

from flask import render_template, request
from jinja2 import TemplateNotFound

from trytond.transaction import Transaction

from functools import wraps
from datetime import datetime, date

turnos_webhook = []

@blueprint.route('/turnos', methods=['GET','POST'])
@tryton.transaction()
def webhook():
    print('webhook')
    Appointment = tryton.pool.get('gnuhealth.appointment')
    Institution = tryton.pool.get('gnuhealth.institution')

    start = datetime.today().replace(hour=0,minute=0,second=0)
    final = datetime.today().replace(hour=23,minute=59,second=59)
    institutions = Institution.search([])
    organization = institutions[0].name.name
    #turnos otorgados el día de hoy
    appointments = Appointment.search([
            ('appointment_date','>=',start),
            ('appointment_date','<',final),
            ('state','!=','free')
            ])
    total = len(appointments)
    #-- desde aca trabajamos con webhooks
    print('antes del request.json')
    try:
        recibido = request.json
    except:
        print('dentro del except')
        recibido = None
    turnos_json_path = 'app/base/static/js/turnos.json'
    print('antes del recibido')
    if recibido != None:
        appointment_calling = recibido['appointment']
        count=0
        for i in range(0,len(turnos_webhook)):
            print('dentro de turnos webhook')
            print(turnos_webhook[i])
            if appointment_calling.get('professional') == turnos_webhook[i].get('professional'):
                count+=1
                if appointment_calling.get('state') == "no_show" \
                    or appointment_calling.get('state') == "end":
                    turnos_webhook.pop(i)
                else:
                    turnos_webhook.pop(i)
                    turnos_webhook.append(appointment_calling)

        if count == 0:
            turnos_webhook.append(appointment_calling)

    #removemos el archivo json de turnos
    if path.exists(turnos_json_path):
        remove(turnos_json_path)

    #creamos un archivo json con los turnos
    print('antes del with open')
    with open(turnos_json_path, 'w') as fp:
        json.dump(turnos_webhook, fp, ensure_ascii=False)

    today = datetime.today().date().strftime('%d %m %Y')
    print('antes del render')
    return render_template('layouts/appointments.html',
                           today=today,
                           turnos_webhook=turnos_webhook,
                           organization=organization,
                           total=total)

@blueprint.route('/appointments2')
@tryton.transaction
def images():
    Configuration = trytond.pool.get('gnuhealth.appointment.screen.configuration')
    config = Configuration(1)
    images = config.images and [c for c in c.images if c.activate] or None
    length = config.images and len(config.images) or 0
    return render_template(
        'appointments2.html',
        images=images, length=length)
